"""

"""

import sys
import pygame
import pygame.display
import pygame.draw
import pygame.font

font = None
labels = []
offset = 0
sdl = None

def format (texts, width):
    """
    Returns a list of labels matching the text in 'width' pixels.
    """

    fit = 0
    labels = []
    tmp = ''
    all = ''
    print('texts: "' + texts + '"')
    
    # loop over all words
    for text in texts.split ( ' ' ):
        tmp = all

        if all == '':
            test = text
        else:
            test =  all + ' ' + text

        x, y = font.size ( test )

        #print "text: " + tmp + text
        # too wide?
        if x > width:
            # 
            print("label: " + all)
            labels.append ( font.render ( all, 1, (255, 255, 128) ) )
            all = text
            
        else:
            # it's ok, append
            all = all + ' ' + text 
        
    # still something in 'tmp'
    if all != '':
        labels.append ( font.render ( all, 1, (255, 255, 128) ) )
        print("label: " + all)
    
    return labels


def paint ( width, height):
    global labels, offset

    # fill
    sdl.fill ( (0,0,0), ( 11, 11, width, height ))

    # draw a frame
    pygame.draw.line ( sdl, (255, 160, 255), (10, 10), (10 + width, 10 ) )
    pygame.draw.line ( sdl, (255, 160, 255), (10 + width, 10 ), (10 + width, 10 + height ) )
    pygame.draw.line ( sdl, (255, 160, 255), (10 + width, 10 + height ), (10, 10 + height ) )
    pygame.draw.line ( sdl, (255, 160, 255), (10, 10 + height ), (10, 10 ) )

    # draw "buttons"
    basex = 10 + width
    basey = 10
    w = 20
    h = 20
    
    pygame.draw.line ( sdl, (255, 160, 255), (basex, basey), (basex + w, basey ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex + w, basey ), (basex + w, basey + h ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex + w, basey + h ), (basex, basey + h ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex, basey + h ), (basex, basey ) )

    basex = 10 + width
    basey = 10 + height - 20

    pygame.draw.line ( sdl, (255, 160, 255), (basex, basey), (basex + w, basey ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex + w, basey ), (basex + w, basey + h ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex + w, basey + h ), (basex, basey + h ) )
    pygame.draw.line ( sdl, (255, 160, 255), (basex, basey + h ), (basex, basey ) )

 
    y = 10
    
    for index in range ( offset, len (labels) ):
        label = labels[index]
        
        # blit out the label
        sdl.blit ( label, ( 10, y ) )
        y += label.get_height () 

        if y >= height:
            break
       
    pygame.display.update ()

 
def main ():
    global font, labels, offset, sdl
    
    pygame.init ()
    sdl = pygame.display.set_mode ( [ 640, 480 ], 0, 24 )  
    font = pygame.font.Font ( '../../fonts/lucida.ttf', 10 )

    # get the width and height
    width = int ( sys.argv[1] )
    height = int ( sys.argv[2] )
   
    text = ' '.join (sys.argv[3:])

    for i in range (10):
        for label in format ( text + str(i), width ):
            labels.append ( label )

    # initial repaint
    paint ( width, height)
    
    # loop forever
    while 1:

        # inner almost forever loop. This loops and picks out all pygame events
        while 1:
            # get next event
            event = pygame.event.wait ()

            # mouse pressed?
            if event.type == MOUSEBUTTONDOWN and event.button == 3:
                pygame.quit ()
                sys.exit ()

            # left mouse pressed?
            if event.type == MOUSEBUTTONDOWN and event.button == 1:
                x, y = event.pos

                if 10 + width < x <  30 + width and 10 < y < 30:
                    print("scroll down")
                    if offset > 0:
                        offset -= 1
                    
                if 10 + width < x <  30 + width and height - 10 < y < height + 10:
                    print("scroll up")
                    if offset < len(labels) -1 :
                        offset += 1

                paint (width, height)
                
if __name__ == '__main__':
    main ()
