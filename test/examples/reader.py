"""

"""

import string
from threading import *

class Reader (Thread):

    def __init__(self, socket):
        """
        Initializes a thread that will connect to the given 'host' on the port 'port'.
        """
        # call superclass
        Thread.__init__ (self)

        # store the socket
        self.socket = socket

 
    def run (self):
        """
        Initial method that starts the thread. This is the entry point into the new thread.
        """

        print("reader: waiting for data")
        
        # make a file from the socket
        file = self.socket.makefile ( 'r' )
      
        while 1:
            datap = string.strip ( file.readline () )

            # did we get anything
            if not data or data == "quit":
                self.socket.send (  "quit\n" )
                break

            print("read: " + data)
            
        print("reader done")
        self.socket.close ()
