"""

"""

import gzip

out = gzip.GzipFile ( "test.gz", "wb" )

out.write ( "hello world 1\n" )
out.write ( "hello world 2\n" )
out.write ( "hello world 3\n" )

out.close ()

infile = gzip.GzipFile ( "test.gz", "rb" )

print(infile.readlines ())
