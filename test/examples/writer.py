"""

"""

from threading import *

class Writer (Thread):

    def __init__(self, socket):
        """
        Initializes a thread that will connect to the given 'host' on the port 'port'.
        """
        # call superclass
        Thread.__init__ (self)

        # store the socket
        self.socket = socket

 
    def run (self):
        """
        Initial method that starts the thread. This is the entry point into the new thread.
        """

        # loop forever
        while 1:
            # read a line of data
            data = input( "input: ")

            self.socket.send ( data + "\n" )
            
            # should we quit?
            if data == "quit":
                break

        print("writer done")
        #self.socket.close()
