"""

"""

import pygame
import pygame.mixer

def main ():

    pygame.init ()

    full   = FULLSCREEN | HWSURFACE | DOUBLEBUF
    window = HWSURFACE | DOUBLEBUF
    
    # driver name
#    print "Driver name: " + pygame.display.get_driver()

    # parameters
#    print "Surfaces in video memory:              " + str ( pygame.video_info ( INFO_HW_A ) ) 
#    print "Window manager present:                " + str ( pygame.video_info ( INFO_WM_A )  ) 
#    print "Video blits accelerated:               " + str ( pygame.video_info ( INFO_BLT_HW ) ) 
#    print "Colorkeys accelerated:                 " + str ( pygame.video_info ( INFO_BLT_HW_CC ) ) 
#    print "Alpha accelerated:                     " + str ( pygame.video_info ( INFO_BLT_HW_A ) ) 
#    print "Memory to video blits accelerated:     " + str ( pygame.video_info ( INFO_BLT_SW ) ) 
#    print "Memory to video colorkeys accelerated: " + str ( pygame.video_info ( INFO_BLT_SW_CC ) ) 
#    print "Memory to video alpha accelerated:     " + str ( pygame.video_info ( INFO_BLT_SW_A ) ) 
#    print "Fills accelerated:                     " + str ( pygame.video_info ( INFO_BLT_FILL ) )
#    print "Amount of video memory:                " + str ( pygame.video_info ( INFO_MEM ) ) 
    # test some resolutions
    print("fullscreen  640 x 480 x 16: " + str ( pygame.display.mode_ok( [ 640, 480], full, 16) ))
    print("fullscreen  640 x 480 x 24: " + str ( pygame.display.mode_ok( [ 640, 480], full, 24) ))
    print("fullscreen  640 x 480 x 32: " + str ( pygame.display.mode_ok( [ 640, 480], full, 32) ))
    print("fullscreen  800 x 600 x 16: " + str ( pygame.display.mode_ok( [ 800, 600], full, 16) ))
    print("fullscreen  800 x 600 x 24: " + str ( pygame.display.mode_ok( [1024, 768], full, 24) ))
    print("fullscreen  800 x 600 x 32: " + str ( pygame.display.mode_ok( [1024, 768], full, 32) ))
    print("fullscreen 1024 x 768 x 16: " + str ( pygame.display.mode_ok( [1024, 768], full, 16) ))
    print("fullscreen 1024 x 768 x 24: " + str ( pygame.display.mode_ok( [1024, 768], full, 24) ))
    print("fullscreen 1024 x 768 x 32: " + str ( pygame.display.mode_ok( [1024, 768], full, 32) ))
                                                                
    print("windowed    640 x 480 x 16: " + str ( pygame.display.mode_ok( [ 640, 480], window, 16) ))
    print("windowed    640 x 480 x 24: " + str ( pygame.display.mode_ok( [ 640, 480], window, 24) ))
    print("windowed    640 x 480 x 32: " + str ( pygame.display.mode_ok( [ 640, 480], window, 32) ))
    print("windowed    800 x 600 x 16: " + str ( pygame.display.mode_ok( [ 800, 600], window, 16) ))
    print("windowed    800 x 600 x 24: " + str ( pygame.display.mode_ok( [1024, 768], window, 24) ))
    print("windowed    800 x 600 x 32: " + str ( pygame.display.mode_ok( [1024, 768], window, 32) ))
    print("windowed   1024 x 768 x 16: " + str ( pygame.display.mode_ok( [1024, 768], window, 16) ))
    print("windowed   1024 x 768 x 24: " + str ( pygame.display.mode_ok( [1024, 768], window, 24) ))
    print("windowed   1024 x 768 x 32: " + str ( pygame.display.mode_ok( [1024, 768], window, 32) ))

    sdl = pygame.display.set_mode ( [ 640, 480 ], 0, 24 )  

    pygame.mixer_music.load ( '../../sound/SomethingForJan.xm' )
    #pygame.mixer_music.load ( '../../sound/PAIN.MOD' )
    pygame.mixer_music.play ( -1 )

    s = ""
    
    # loop forever
    while 1:
        # get next event
        event = pygame.event.wait ()

        print(event)
            
        # left mouse pressed?
        if event.type == MOUSEBUTTONDOWN and event.button == 1:
            pygame.quit ()
            break

        if event.type == KEYDOWN:
            s += event.str
            print(event.key, pygame.key.name ( event.key))

            print(s)
            
if __name__ == '__main__':
    main ()
