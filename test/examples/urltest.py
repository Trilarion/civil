"""

"""

import re
import sys
import urllib.request, urllib.parse, urllib.error

#from xml.dom.utils import FileReader
#from xml.dom.ext.reader.Sax2 import FromXml

f = urllib.request.urlopen ( sys.argv[1] )

scenario = re.compile ( "^\s*http://.+\.xml\s*$" )

for line in f.readlines ():
    line = line.strip ()

    # does this line match?
    if scenario.search ( line ):
        print("scen: " + line)
    else:
        print("line: " + line)
        
