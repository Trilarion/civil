"""

"""

import time
from civil.serialization.scenario_manager import ScenarioManager

start = time.time ()

sm = ScenarioManager ()

sm.loadAllScenarioInfos ( '../scenarios/' )


stop = time.time ()

print(sm.scenarios)

print("time elapsed: %f" % (stop - start))
