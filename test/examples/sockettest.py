"""

"""

import sys
import socket

from reader import *
from writer import *


class Client (Thread):

    def __init__(self, host, port):
        """
        Initializes a thread that will connect to the given 'host' on the port 'port'.
        """
        # call superclass
        Thread.__init__ (self)

        # store the host and port
        self.host = host
        self.port = port

 
    def run (self):
        """
        Initial method that starts the thread. This is the entry point into the new thread.
        """
        try:
            # create the socket
            self.socket = socket.socket()

            # connect to the remote system
            self.socket.connect ( ( self.host, self.port ) )
        except:
            print("error creating socket: " + repr(sys.exc_info()[1]))
            return

        for i in range(100):
            try:
                self.socket.send ( 'foo\n' )
            except:
                print("error sending data.")
                return

        print("client done")
        self.socket.close()


def initServer ():
    print("initServer()")
    
    # create the socket
    try:
        s = socket.socket()

        # allow bind()ing while a previous socket is still in TIME_WAIT.
        s.setsockopt ( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

        # bind our socket for incoming requests
        s.bind ( ('localhost', 50000) )
    except:
        print("error creating socket: " + repr(sys.exc_info()[0]))
        return

    try:
        # listen for clients
        s.listen ( 1 )
        
        # accept anything that arrives
        connection, addr = s.accept ()

        # return the new socket
        return connection
    except:
        print("error listening on socket: " + repr(sys.exc_info()[0]))
        return None
    

def initClient ():
    print("initClient()")
    
    try:
        # create the socket
        s = socket.socket()
        
        # connect to the remote system
        s.connect ( ( 'localhost', 50000 ) )

        return s
    except:
        print("error creating socket: " + repr(sys.exc_info()[1]))
        return None



if __name__ == '__main__':
    if len ( sys.argv ) < 2:
        print("usage: %s server|client" % sys.argv[0])
        sys.exit (1)
        
    # are we a server?
    if sys.argv[1] == "server":
        # run as a server
        s = initServer ()
           
    # are we a client?
    elif sys.argv[1] == "client":
        # run as a client
        s = initClient ()
 
    else:
        print("usage: %s server|client" % sys.argv[0])
        sys.exit (1)

    # did we get a socket or not?
    if s:
        # yep, create a reader and a writer
        Reader ( s ).start ()
        Writer ( s ).start ()
    else:
        print("could not init socket")
