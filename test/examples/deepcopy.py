"""

"""

import copy

class foo:
    def __init__ (self, a):
        self.a = a
        self.b = [1,2,3]

    def __str__(self):
        return str(self.a) + ' ' + str( self.b)
    
    def __deepcopy__(self, memo):
        obj = foo(self.a)
        memo[id(self)] = obj
        for k,v in list(self.__dict__.items()):
            print(k, type(k))
            obj.__dict__[k] = copy.deepcopy(v, memo)

        return obj

## def __deepcopy__(self,memo):
##         copy.deepcopy ( self.a, memo )
##         copy.deepcopy ( self.b, memo )
##         print memo

f = foo(10)
g = copy.deepcopy ( f )

f.a += 1
f.b = 10
print(f)
print(g)
print(f)
    
