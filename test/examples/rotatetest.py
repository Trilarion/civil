"""

"""

import math
import string
import sys

if __name__ == '__main__':

    size_x = 10
    size_y = 10
    
    # create a orig
    orig = [ None ] * size_x
    for index in range (size_y):
        orig[index] = [ None ] * size_y

    dest = [ None ] * size_x
    for index in range (size_y):
        dest[index] = [ None ] * size_y

    # loop again and set some points
    for x in range (0, size_x):
        for y in range (0, size_y):
            # set the point value
            if y > 1 and y < 8 and x > 1 and x < 8:
                orig[x][y] = '*'
            else:
                orig[x][y] = '.'

            # fill in the destination too
            dest[x][y] = '.'
            
    angle = float (sys.argv[1])
    coeff = (angle / 180.0) * math.pi

    print("Rotating: " + str(angle) + " degrees (rad: " + str(coeff) + ")")


    # loop again and rotate
    for x in range ( -5, 5 ):
        for y in range ( -5, 5 ):
            # get the original point value
            value = orig[x + 5][y + 5]

            # calculate the new x and y values
            newx = int ( x * math.sin (coeff) - y * math.cos(coeff) ) + 5
            newy = int ( y * math.sin (coeff) + x * math.cos(coeff) ) + 5

            print(x, y, " -> ", newx, newy)
            #print x* math.cos ( coeff ), y * math.sin ( coeff )
            
            # is the point valid?
            if newx >= 0 and newx < 10 and newy >= 0 and newy < 10:
                # set point in destination matrix
                dest [ newx ][ newy ] = value
#            else:
#                print newx, newy
                
    for y in range (0, size_y):
        print(string.join (orig[y], ' '))

    print()
    
    for y in range (0, size_y):
        print(string.join (dest[y], ' '))


# Rotation around ORIGO in 2d:
# 
#      rx = bx * sin(angle) - by * cos(angle); 
#      ry = by * sin(angle) + bx * cos(angle); 
# where (bx,by) are the base coordinates (coordinates before rotation) and
# (rx,ry) are the rotated coordinates. The mathematical functions sin and cos
# wants the angle in radians, but we are used to count angles in degrees. One
# revolution in a circle is 360 degrees and it is 2*PI radians. This
# information tells us that:
# 
#      1 degree = (1/360)*2*PI radians
