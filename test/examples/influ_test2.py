"""

"""

#!/usr/bin/env python

#MAP_SHAPE = 'diamond_v' # diamond shaped - stacked vertically
#MAP_SHAPE = 'diamond_h' # diamond shaped - stacked horizontally
MAP_SHAPE = 'rectangle' # array like layout

import os
os.chdir('../..')

from Numeric import *

import sys
from civil.map.hex import Hex
from civil.map.map import Map
from whrandom import randint
from ai.influence_map2 import InfluenceMap

# map display formatting functions
red = "\033[1;31m"
dred = "\033[0;31m"
green = "\033[1;32m"
dgreen = "\033[0;32m"
yellow = "\033[1;33m"
dyellow = "\033[0;33m"
magenta = "\033[0;35m"
blue = "\033[0;34m"
normal = "\033[0m"
def hl(xy): return "%s%s%s" % (magenta,xy,normal)
def sprint(text): sys.stdout.write(text)


class ShowMap(InfluenceMap):

    def loadTestMap(self,test_map):
        hex_map = self.hex_map
        units = []
        for y in range(hex_map.size[1]):
            line = test_map[y]
            for x in range(hex_map.size[0]):
                if line[x] == 'x': units.append((x,y,4.))
                elif line[x] == 'o': units.append((x,y,-4.))
                elif line[x] == 'X': units.append((x,y,8.))
                elif line[x] == 'O': units.append((x,y,-8.))
        self.setUnitMap(units)
                    
    def show_map(self):
        size_x = self.hex_map.size[0]
        for x in range(size_x):
            sprint(" / \\")
        print()
        self.show_rows()
        mx = 0.0
        for row in self.weight_map:
            m = max(row)
            if (abs(mx) < abs(m)): # and m != 4.0 and m != 8.0:
                mx = m
        print(mx)
        print(self.hex_map.size)

    def show_rows(self,y=0,odd=0,me=None):
        size_x, size_y = self.hex_map.size
        if size_y == y: return
        if odd: 
            print("  ", end=' ')
        sprint("|")
        for x in range(size_x):
            self.coord_print(x,y)
        print()
        if odd: 
            print(" /", end=' ')
        for x in range(size_x):
            sprint(" \ /")
        if odd: 
            odd = 0
        else:
            sprint(" \\")
            odd=1
        print()
        if not me:
            me = self.show_rows
        me(y+1,odd,me)

    def coord_print(self,x,y):
        weight_map = self.weight_map
        format = None
        if weight_map[y,x] > -0.1 and  weight_map[y,x] < 0.1:
            if (x == 0 and y < 10) or (y==0 and x<10):
                sprint("%s,%s|" % (x,y))
            else:
                sprint("   |")
        else:
            weight = self.weight_map[y,x]
            format = "%.1f"
            if weight > 0:
                color = green
                orig = dgreen
            else:
                color = dred
                orig = red
            weight = abs(weight)
            if self.const_map[y,x] != 0:
                sprint("%s%s%s|" % (orig,format % weight,normal))
            else:
                sprint("%s%s%s|" % (color,format % weight,normal))

test_map = [
"                  ",
" XX               ",
" xx               ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"    O             ",
"                  ",
"                  " ]

test_map2 = [
"                  ",
"   X X            ",
"                  ",
"                  ",
"                  ",
" xx               ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"        O         ",
"                  ",
"                  ",
"                  " ]

test_map3 = [
"                  ",
"                  ",
"                  ",
"     X X          ",
"                  ",
"                  ",
"                  ",
"                  ",
"   x x            ",
"                  ",
"                  ",
"                  ",
"                  ",
"            O     ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  ",
"                  " ]

atest_map = [
"                    ",
"                    ",
"                    ",
"         O          ", # odd
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"                    ",
"         X          ", # even
"                    ",
"                    ",
"                    ",
"                    ",
"                    " ]

if __name__ == "__main__":
    class TestMap:
        size = (len(test_map[0]),len(test_map))
    hex_map = TestMap()
    iterations = None
    imap = ShowMap(hex_map)
    imap.setDecayRate(0.5)

    imap.loadTestMap(test_map)
#    imap._iterations = 5
#    import profile
#    def prof_test(imap=imap):
#        for x in range(10000):
#            imap.reset()
#            imap.step()
#    profile.run('prof_test()')
    imap.step(iterations)
    imap.show_map()
    
    imap.loadTestMap(test_map2) 
    imap.step(iterations) 
    imap.show_map()
#    
    imap.loadTestMap(test_map3) 
    imap.step(iterations) 
    imap.show_map()

#test_map = [
#"               ",
#"               ",
#"               ",
#"               ",
#"  x    x       ",
#"   X           ",
#"  x x          ",
#" x             ",
#"               ",
#"               ",
#"               ",
#"               ",
#"               ",
#"       o       ",
#"               ",
#"               ",
#"               ",
#"               " ]


