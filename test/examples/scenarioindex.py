"""

"""

import time

from civil.serialization.scenario_manager import ScenarioManager

start = time.time ()

sm = ScenarioManager ()


data = sm.loadScenarioIndex ( "../scenarios/scenarioindex.xml" )

stop = time.time ()

print("time elapsed: %f" % (stop - start))
