"""

"""

import sys
from math import floor


def loopRotate (start, stop, steps_per_turn, delay, id, results):
    """
    Performs the loop that creates the facing changes for each turn. Multiple commands are
    created until the turn is complete, and placed on the 'results' hash. The parameters are
    quite stratightforward.
    """

    distance = 0
    tmp = start
    while tmp != stop:
        distance += 1
        tmp += 1

        if tmp == 36:
            tmp = 0

    if distance > 18:
        #steps_per_turn = -steps_per_turn
        distance = 36 - distance
        up = 0
        print("counterclockwise:", 36 - distance)

    else:
        up = 1
        print("clockwise:", distance)

    rotated = 0
    last = 100
        
    while rotated < distance:
        # add the steps for this turn
        rotated += steps_per_turn
        
        # this happens the next turn
        delay += 1
        
        if up:
            facing = start + rotated

            if facing > 35:
                facing = facing - 36

        else:
            facing = start - rotated

            if facing < 0:
                facing = 36 + facing

        if floor (facing) != last:
            # send the rotation out from the engine
            #results.add ( delay,  Rotate ( id, delay, int ( floor ( facing ) ) ))
            print("delay: %2d, facing: %2d, rotated: %2d" % (delay,facing,rotated))

        last = floor (facing)

loopRotate ( int(sys.argv[1]), int(sys.argv[2]), 0.5, 0, 100, 1)
