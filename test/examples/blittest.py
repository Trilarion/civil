"""

"""

import time

import pygame
import pygame.time


def main ():

    pygame.init ()

    sdl = pygame.display.set_mode ( [ 1024, 768 ], DOUBLEBUF | HWSURFACE, 24 )  

    icon1 = pygame.Surface ( (32, 32) )
    icon2 = pygame.Surface ( (1024, 768) )

    icon1.set_at ( (31, 31), (255, 255, 255))

    for x in range (32):
        for y in range (24):
            icon2.set_at ( (x*32, y*32), (255, 255, 0))

    icon1 = icon1.convert ()
    icon2 = icon2.convert ()

    start1 = time.time ()

    for index in range ( 100 ):
        for x in range (32):
            for y in range (24):
                sdl.blit ( icon1, (x*32, y*32))

        pygame.display.flip ()

    stop1=time.time ()

    print(stop1 - start1)

    start2 = time.time ()

    for index in range ( 100 ):
        sdl.blit ( icon2, (0,0) )

    pygame.display.flip ()

    stop2=time.time ()

    print(stop2 - start2)
    
    # loop forever
    while 1:
        # get next event
        event = pygame.event.wait ()
            
        # left mouse pressed?
        if event.type == MOUSEBUTTONDOWN:
            pygame.quit ()
            break

           
if __name__ == '__main__':
    main ()
