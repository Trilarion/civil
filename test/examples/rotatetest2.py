"""

"""

from math import floor, ceil

basedelay = 7
start = 20
stop  = 30
speed = 20
up = 1
turns_per_minute = 5.0

steps_per_turn = float(speed) / float(turns_per_minute)

if stop < start:
    steps_per_turn = -steps_per_turn
    up = 0
tmp = start
sec = basedelay

while (up and tmp < stop) or (not up and tmp >= stop):
    sec += 1

    new =  tmp + steps_per_turn
    
    # would we advance one step?
    if  (steps_per_turn > 0 and floor (new) > tmp) or (steps_per_turn < 0 and ceil (new) < tmp):
        # we advance one
        print("at:", sec, "moving to:", int (floor (new)))

    tmp = new
    
#print steps_per_sec

