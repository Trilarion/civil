"""

"""

import pygame
import pygame.time

def wait ():
    # loop forever
    while 1:
        # get next event
        event = pygame.event.wait ()
        
        # left mouse pressed?
        if event.type == MOUSEBUTTONDOWN:
            break

  
def main ():

    pygame.init ()

    sdl = pygame.display.set_mode ( [ 1024, 768 ], DOUBLEBUF | HWSURFACE, 24 )  

    icon1 = pygame.Surface ( (32, 32) )
    icon1.set_at ( (31, 31), (255, 255, 255))
    icon1 = icon1.convert ()

    for x in range (32):
        for y in range (24):
            sdl.blit ( icon1, (x*32, y*32))
            
    pygame.display.flip ()

    wait ()

    sdl = pygame.display.set_mode ( [ 1280, 1024 ], DOUBLEBUF | HWSURFACE, 24 ) 


    for x in range (40):
        for y in range (32):
            sdl.blit ( icon1, (x*32, y*32))
            
    pygame.display.flip ()

    wait ()
   
if __name__ == '__main__':
    main ()
