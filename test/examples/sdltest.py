"""

"""

import pygame
import pygame.time

def main ():

    pygame.init ()

    sdl = pygame.display.set_mode ( [ 640, 480 ], 0, 24 )  

    pygame.key.set_repeat (200,20)
    
    s = ""
    pygame.time.set_timer ( pygame.USEREVENT + 1, 50 )
    
    # loop forever
    while 1:
        # get next event
        event = pygame.event.wait ()

        print(event)
            
        # left mouse pressed?
        if event.type == MOUSEBUTTONDOWN and event.button == 1:
            pygame.quit ()
            break

        if event.type == KEYDOWN:
            s += event.str
            print(event.key, pygame.key.name ( event.key))

            print(s)
            
if __name__ == '__main__':
    main ()
