"""

"""

#!/usr/bin/env python

#MAP_SHAPE = 'diamond_v' # diamond shaped - stacked vertically
#MAP_SHAPE = 'diamond_h' # diamond shaped - stacked horizontally
MAP_SHAPE = 'rectangle' # array like layout

import os
os.chdir('../..')

import sys
from civil.map.hex import Hex
from civil.map.map import Map
from whrandom import randint
from ai import basic_path
test_path = basic_path
#from ai import num2_path
#test_path = num2_path

# map display formatting functions
red = "\033[0;31m"
green = "\033[1;32m"
dgreen = "\033[0;32m"
yellow = "\033[1;33m"
dyellow = "\033[0;33m"
magenta = "\033[0;35m"
blue = "\033[0;34m"
normal = "\033[0m"
def hl(xy): return "%s%s%s" % (magenta,xy,normal)
def sprint(text): sys.stdout.write(text)

### methods to display map and path
class DbgPath(test_path.PathFinder):

    def __init__(self,*args):
        self.tc_map = []
        return test_path.PathFinder.__init__(self,*args)

    def show_path(self,test_prob):
        self.path,self.skip_nodes = \
                self.calculatePath(_test=1,transform=0,*test_prob)
        self.show_map()
        
    def show_map(self):
        size_x = self.map.size[0]
        for x in range(size_x):
            sprint(" / \\")
        print()
        self.show_rows()
        print("Path Length: %s" % len(self.path))

    def show_rows(self,y=0,odd=0,me=None):
        size_x, size_y = self.map.getSize()
        if size_y == y: return y
        if odd: 
            print("  ", end=' ')
        sprint("|");
        for x in range(size_x):
            # on small maps show coordinates
            if size_x > 10 or size_y > 10:
                self.big_coord_print(x,y)
            else:
                self.coord_print(x,y)
        print()
        if odd: 
            print(" /", end=' ')
        for x in range(size_x):
            sprint(" \ /")
        if odd: 
            odd = 0
        else:
            sprint(" \\")
            odd=1
        print()
        if not me:
            me = self.show_rows
        return me(y+1,odd,me)

    def coord_print(self,x,y):
        t_cost = self.tc_map[y][x]
        if t_cost > 0.9: thl = green
        elif t_cost > .7: thl = dgreen
        elif t_cost > .5: thl = yellow
        elif t_cost > .3: thl = dyellow
        elif t_cost < 0.2: thl = blue
        else: thl = red
        if (x,y) in self.path: 
            sprint("%s%s,%s%s|" % (hl(x),thl,normal,hl(y)))
        else:
            sprint("%s%s,%s%s|" % (thl,x,y,normal))

    def big_coord_print(self,x,y):
        t_cost = self.tc_map[x][y]
        if t_cost > 0.9: thl = green
        elif t_cost > .7: thl = dgreen
        elif t_cost > .5: thl = yellow
        elif t_cost > .3: thl = dyellow
        elif t_cost < 0.2: thl = blue
        else: thl = red
        if (x,y) in self.path: 
            sprint("%s|%s%s%s|%s|" % (thl,normal,hl('*'),thl,normal))
        elif (x,y) in self.skip_nodes: 
            sprint("%s|#|%s|" % (thl,normal))
        else:
            sprint(" %s#%s |" % (thl,normal))

# These are point sets that I found useful for finding problems with my
# implementation. 
prob1 = ((4,1),(1,3))
prob2 = ((4,1),(0,7))
prob3 = ((0,7),(4,1))
prob4 = ((3,18),(20,3))
prob5 = ((2,2),(10,10))
prob6 = ((4,1),(40,7))
prob7 = ((4,0),(23,17))
prob8 = ((4,20),(55,47))
prob9 = ((2,70),(65,17))


# create the test environ... change map size here
#sx,sy = (6,10)
#sx,sy = (124,124)
#sx,sy = (24,24)
sx,sy = (12,12)
#sx,sy = (34,34)
test_prob = prob5

# r = road
# f = fields
# m = mount
# t = forest
ter = {'f':1,'t':230,'m':123}

# 12x12
map1 = [
"ffffffffffff",
"ffffffffffff",
"ffffffffffff",
"ffmmffffffff",
"fmmmmfffffff",
"fmmmmmmfffff",
"fmmmmmmfffff",
"ffmmmmmffftt",
"fttttttttttt",
"ftfttttftttt",
"tffttttttttt",
"ffffffffffff",
"tttttttttttt"
]
maps = []
from civil.model import scenario

for i in range(1): 
    scenario.map = map = Map(sx, sy) 
    hexes = map.getHexes() 
    for y in range(len(hexes)): 
        for x in range(len(hexes[y])): 
            if (x,y) in test_prob:
                hexes[y][x]=Hex(1) 
            else: 
                hexes[y][x]=Hex(ter[map1[x][y]])
#                hexes[y][x]=Hex(randint(1,487))
    path = DbgPath(map) 
    maps.append(path)

# display's a ascii map
# key: red|yellow|green = high/medium/low terrain costs
# magenta is used to show the path
path.show_path(test_prob)
print(path.path)
#print 'path length:',len(path.calculatePath())
#import time
#time.sleep(3)
# profiling: just run the path calculation method
def test(maps=maps):
    while maps:
        i = randint(0,len(maps)-1)
        path = maps[i]
#        path.show_path()
        path.calculatePath(transform=0,*test_prob)
#        print 'path length:',len(path.calculatePath(transform=0,*test_prob))
        del maps[i]
#profile.run("test()")




