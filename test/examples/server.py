"""

"""

import string
import socket
from threading import *


class Server (Thread):

    def __init__(self, port):
        """
        Initializes a thread that should listen on the port 'port'.
        """
        # call superclass
        Thread.__init__ (self)

        # store the port
        self.port = port
        
 
    def run (self):
        """
        Initial method that starts the thread. This is the entry point into the new thread.
        """

        # create the socket
        try:
            self.socket = socket.socket()

            # allow bind()ing while a previous socket is still in TIME_WAIT.
            s.setsockopt ( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

            # bind our socket for incoming requests
            self.socket.bind ( ('localhost', self.port) )
        except:
            print("error creating socket: " + repr(sys.exc_info()[0]))
            return

        try:
            # listen for clients
            self.socket.listen ( 1 )

            # accept anything that arrives
            self.connection, addr = self.socket.accept ()
        
            print("connection from: " + str(addr))
            # make a file from the socket
            file = self.connection.makefile ( 'r' )
        except:
            print("error accepting new client.")
            return


        while 1:
            data = string.strip ( file.readline () )
            #data = self.connection.recv (5)
            if not data:
                break
            print("read: " + data)
            
        print("server done")
        self.connection.close ()

            
class Client (Thread):

    def __init__(self, host, port):
        """
        Initializes a thread that will connect to the given 'host' on the port 'port'.
        """
        # call superclass
        Thread.__init__ (self)

        # store the host and port
        self.host = host
        self.port = port

 
    def run (self):
        """
        Initial method that starts the thread. This is the entry point into the new thread.
        """
        try:
            # create the socket
            self.socket = socket.socket()

            # connect to the remote system
            self.socket.connect( ( self.host, self.port ) )
        except:
            print("error creating socket: " + repr(sys.exc_info()[1]))
            return

        for i in range(100):
            try:
                self.socket.send ( 'foo\n' )
            except:
                print("error sending data.")
                return

        print("client done")
        self.socket.close()
        


if __name__ == '__main__':
    Server ( 50000 ).start()
    Client ( 'localhost', 50000 ).start()
