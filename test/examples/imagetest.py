"""

"""

import sys
import pygame
import pygame.time


def main ():

    pygame.init ()

    # load the triangle
    triangle = pygame.image.load ( sys.argv [1] )

    # now loop over the entire map matrix 
    for y in range ( 16 ):
        print("(", end=' ')
        for x in range ( 16 ):
            # get the pixel in the triangle
            color = triangle.get_at ( (x, y) )

            # transparent?
            if color[0] == 255:
                print("0,", end=' ')

            else:
                print("1,", end=' ')

        print("),")
           
if __name__ == '__main__':
    main ()
