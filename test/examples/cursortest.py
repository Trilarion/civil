"""

"""

import sys

import pygame
import pygame.cursors
import pygame.display
import pygame.mouse


def load_xbm(curs, mask):
    """
    pygame.cursors.load_xbm(cursorfile, maskfile) -> cursor_args
reads a pair of XBM files into set_cursor arguments

Arguments can either be file names or filelike objects
with the readlines method. Not largely tested, but
should work with typical XBM files.
"""
    def read_size(data):
        # split up the first lines into the three parts
        width = data[0].split()
        height = data[1].split()
        
        # precautions
        if len (width) != 3 or len (height) != 3 or width[0] != '#define' or height[0] != '#define':
            # invalid files
            raise ValueError ( 'invalid xbm file' )

        try:
            return ( int (width[2]), int (height[2] ) )
        except:
            raise ValueError ( 'invalid xbm dimensions' )

    def read_hotspot(data):
        # split up the lines 2 and 3 into the three parts
        x = data[2].split()
        y = data[3].split()

        # precautions
        if len (x) != 3 or len (y) != 3 or x[0] != '#define' or y[0] != '#define':
            # no hotspot
            return (0,0)

        try:
            return ( int (x[2]), int (y[2] ) )
        except:
            raise ValueError ( 'invalid xbm hotspot' )

    def get_data(data):
        parts = data.split ( '0x' )
        print(len(parts))
        print(parts)
        
    def bitswap(num):
        val = 0
        for b in [num&(1<<x) != 0 for x in range(8)]:
            val = val<<1 | b
        return val
    if type(curs) is type(''): curs = open(curs)
    if type(mask) is type(''): mask = open(mask)
    curs = curs.readlines()
    mask = mask.readlines()

    cursor_size = read_size(curs)
    mask_size = read_size(mask)
    
    # same size for mask and cursor?
    if cursor_size != mask_size:
        raise ValueError ( 'cursor and mask have different sizes' )

    hotspot = read_hotspot(curs)
    print(hotspot)
    all=''
    for line in curs[5:]:
        print(line)
        all +=  line.strip ().replace ('};', '').replace (',','' )

    print(all)
    
    #map (string.strip, line for line in curs[5:])
    
    #all = ','.join ( map (string.strip, line) for line in curs[5:])).replace('};', '')
    
    get_data ( all )
    
    info = tuple([int(curs[x].split()[-1]) for x in range(4)])
    data = ' '.join(curs[5:]).replace('};', '').replace(',', '')
    cursdata = tuple([bitswap(int(x, 16)) for x in data.split()])
    data = ' '.join(mask[5:]).replace('};', '').replace(',', '')
    maskdata = tuple([bitswap(int(x, 16)) for x in data.split()])
    return info[:2], info[2:], cursdata, maskdata

def main ():

    pygame.init ()

    pygame.display.set_mode ( [ 640, 480 ], 0, 24 )  

    #load_xbm ( "/opt/src/deckit-1.1/BWidget/images/minus.xbm",
    #           "/opt/src/deckit-1.1/BWidget/images/minus.xbm" )

    #"../../gfx/pointers/cursors-map-point-001.xbm",
    #"../../gfx/pointers/cursors-map-point-mask-001.xbm" )

    
    # loop forever
    while 1:

        cursor = load_xbm ( "../../gfx/pointers/cursors-map-point-001.xbm",
                            "../../gfx/pointers/cursors-map-point-mask-001.xbm" )

        pygame.mouse.set_cursor( *cursor )

        # inner almost forever loop. This loops and picks out all pygame events
        while 1:
            # get next event
            event = pygame.event.wait ()
 
            # mouse pressed?
            if event.type == MOUSEBUTTONDOWN:
                pygame.quit ()
                sys.exit ()
    
if __name__ == '__main__':
    main ()
