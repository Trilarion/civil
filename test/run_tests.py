"""
Discovers all tests and runs them. Assumes that initially the working directory is test and source is not known
in the sys path.
"""

import os
import sys
import unittest


if __name__ == '__main__':

    # add source directory to path if needed
    tests_directory = os.path.abspath(os.path.dirname(__file__))
    source_directory = os.path.realpath(os.path.join(tests_directory, os.path.pardir, 'source'))
    if source_directory not in sys.path:
        sys.path.insert(0, source_directory)

    loader = unittest.defaultTestLoader

    tests = loader.discover(tests_directory)

    runner = unittest.TextTestRunner()

    results = runner.run(tests)

    # return sum of number of errors and number of failures
    sys.exit(len(results.errors) + len(results.failures))
