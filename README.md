[![Build Status](https://travis-ci.org/Trilarion/civil.svg?branch=master)](https://travis-ci.org/Trilarion/civil)
[![Documentation Status](https://readthedocs.org/projects/civil/badge/?version=latest)](http://civil.readthedocs.io/en/latest/)
[![Download](https://api.bintray.com/packages/trilarion/osg/Civil/images/download.svg)](https://bintray.com/trilarion/osg/Civil/_latestVersion)

# Civil

**[Blog](https://civil-game.blogspot.com/) - [History](http://civil.readthedocs.io/en/latest/history.html) - [Manual](http://civil.readthedocs.io/en/latest/)**

Civil is a real-time strategy game allowing players to take part in scenarios set
during the American Civil war.

It is written in Python 3 and the code is released under the open source [GPL-3.0 license](LICENSE.txt).


## Download & Manual

The latest version is **0.84** released on January 11th, 2018. It's a fix of the old code.

Direct download of [Civil 0.84 Windows Installer](https://bintray.com/trilarion/osg/download_file?file_path=civil-0.84-win.exe).


For information how to play Civil see the [Civil Manual](http://civil.readthedocs.io/en/latest/).

## Installation from source

For Python users, it's on [PyPI](https://pypi.python.org/pypi/civil/0.84).

Download and install from PyPI.

    pip install civil
    
Then start with

    civil_start

## Bugs, Feature requests, Feedback

- Report a bug/feature request/feedback on the [Github issue tracker](https://github.com/Trilarion/civil/issues)
- Give feedback by commenting on the [Blog](https://civil-game.blogspot.com/)

## Contributing

- Report a bug/feature request or give feedback (see above)
- Fork this repository and create a [Github pull request](https://github.com/Trilarion/civil/pulls)
