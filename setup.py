"""
Setup
"""

from setuptools import setup, find_packages
import os
import shutil
import civil.version as vs

HERE = os.path.abspath(os.path.dirname(__file__))

CLASSIFIERS = ['Development Status :: 3 - Alpha',
               'Intended Audience :: End Users/Desktop',
               'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
               'Operating System :: OS Independent',
               'Programming Language :: Python :: 3',
               'Topic :: Games/Entertainment :: Real Time Strategy']

KEYWORDS = 'US Civil war strategy game open source'

def get_package_data_files():
    """
    Get the list of data files (full content of data directory).
    """
    data_files = []
    basepath = os.path.join(HERE, 'source', 'civil')
    for dirpath, dirnames, filenames in os.walk(os.path.join(basepath, 'data')):
        relpath = os.path.relpath(dirpath, basepath)
        names = [os.path.join(relpath, name) for name in filenames]
        data_files.extend(names)
    return {'civil': data_files}


if __name__ == "__main__":

    setup(name='civil',
        version=vs.__version__,
        description='Open source real-time strategy game set during the American Civil war. ',
        long_description='',
        url='https://github.com/Trilarion/civil',
        author='The Civil Development Team',
        author_email='civil@twelvepm.de',
        license='GPL',
        classifiers=CLASSIFIERS,
        keywords=KEYWORDS,
        package_dir={'': 'source'},
        packages=find_packages(where=os.path.join(HERE, 'source')),
        # install_requires=['PyQt5>=5.6', 'PyGame>=1.9'],
        install_requires=['PyGame>=1.9'],
        package_data=get_package_data_files(),
        entry_points={'console_scripts': ['civil_start=civil.civil_start:start']},
        zip_safe=False)
