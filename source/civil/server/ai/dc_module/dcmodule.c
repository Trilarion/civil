
#include <Python.h>
#include <math.h>
#include <stdio.h>

#define sign(n) (n > -1)
#define a2h_x(x, y) (x - floor(y/2.0))
#define a2h_y(x, y) (x + ceil(y/2.0))
#define h2a_x(x, y) (floor((x+y)/2.0))
#define h2a_y(x, y) (y - x)
#define max(x,y) (x>y?x:y)

/* Prototypes */
static PyObject * dc_distance_cost_r(PyObject *self, PyObject *args);
static PyObject * dc_distance_cost_dv(PyObject *self, PyObject *args);
static PyObject * dc_distance_cost_dh(PyObject *self, PyObject *args);
static PyObject * dc_distance_cost_sg(PyObject *self, PyObject *args);
static PyObject * dc_distance_cost_sd(PyObject *self, PyObject *args);
void initdc(void);



static PyObject *
dc_distance_cost_r(self,args)
    PyObject *self;
    PyObject *args;
{
    int *x, *y, *xx, *yy;
    int cost, dx, dy;

    if (!PyArg_ParseTuple(args,"(ii)(ii)",&x,&y,&xx,&yy))
        return NULL;

    dx = a2h_x((int)xx,(int)yy) - a2h_x((int)x,(int)y);
    dy = a2h_y((int)xx,(int)yy) - a2h_y((int)x,(int)y);

    if (sign(dx) == sign(dy))
        cost = max(abs(dx),abs(dy));
    else
        cost = abs(dx)+abs(dy);
    return Py_BuildValue("i",cost);
};

static PyObject *
dc_distance_cost_dv(self,args)
    PyObject *self;
    PyObject *args;
{
    int *x, *y, *xx, *yy;
    int cost, dx, dy;

    if (!PyArg_ParseTuple(args,"(ii)(ii)",&x,&y,&xx,&yy))
        return NULL;

    dx = (int)xx - (int)x;
    dy = (int)yy - (int)y;

    if (sign(dx) == sign(dy))
        cost = max(abs(dx),abs(dy));
    else
        cost = abs(dx)+abs(dy);
    return Py_BuildValue("i",cost);
};

static PyObject *
dc_distance_cost_dh(self,args)
    PyObject *self;
    PyObject *args;
{
    int *x, *y, *xx, *yy;
    int cost, dx, dy;

    if (!PyArg_ParseTuple(args,"(ii)(ii)",&x,&y,&xx,&yy))
        return NULL;

    dx = (int)xx - (int)x;
    dy = (int)yy - (int)y;

    if (sign(dx) != sign(dy))
        cost = max(abs(dx),abs(dy));
    else
        cost = abs(dx)+abs(dy);
    return Py_BuildValue("i",cost);
};

static PyObject *
dc_distance_cost_sg(self,args)
    PyObject *self;
    PyObject *args;
{
    int *x, *y, *xx, *yy;
    int cost, dx, dy;

    if (!PyArg_ParseTuple(args,"(ii)(ii)",&x,&y,&xx,&yy))
        return NULL;

    dx = (int)xx - (int)x;
    dy = (int)yy - (int)y;

    cost = abs(dx)+abs(dy);
    return Py_BuildValue("i",cost);
};


static PyObject *
dc_distance_cost_sd(self,args)
    PyObject *self;
    PyObject *args;
{
    int *x, *y, *xx, *yy;
    int cost, dx, dy;

    if (!PyArg_ParseTuple(args,"(ii)(ii)",&x,&y,&xx,&yy))
        return NULL;

    dx = (int)xx - (int)x;
    dy = (int)yy - (int)y;

    cost = max(abs(dx),abs(dy));
    return Py_BuildValue("i",cost);
};

static PyMethodDef dcMethods[] = {
    {"distance_cost_r",dc_distance_cost_r,METH_VARARGS},
    {"distance_cost_dv",dc_distance_cost_dv,METH_VARARGS},
    {"distance_cost_dh",dc_distance_cost_dh,METH_VARARGS},
    {"distance_cost_sg",dc_distance_cost_sg,METH_VARARGS},
    {"distance_cost_sd",dc_distance_cost_sd,METH_VARARGS},
    {NULL, NULL} /* Sentinel */
};

void initdc(void)
{
    (void) Py_InitModule("dc", dcMethods);
};

