//#include "python_config.h"
#include <Python.h>

/* the internal map data */
static int *map;

static int width, height, sf;

static int rdlosmap(PyObject *ly, int **mem, int *w, int *h) // reads from a python [][]
{
    PyObject *lx, *v;
    int x,y, value;

    *h=PyList_Size(ly);
    lx=PyList_GetItem(ly,0);
    *w=PyList_Size(lx);

    if(((*mem)=(int*)malloc((*w)*(*h)*sizeof(int)))==NULL) {
        printf("ccivil.rdlosmap: malloc error\n");
        return 0;
    }

    /* reads data */
    for(y=0; y<(*h); y++) {
        lx=PyList_GetItem(ly,y);
        for(x=0; x<(*w); x++) {
            v=PyList_GetItem(lx,x);
            value = PyInt_AsLong(v);
            (*mem)[y*(*w) + x]= value;
        }
    }

    return 1;
}

static int rdpixline(PyObject *pixlist, int *mem, int pw)
{
    PyObject *v;
    int x,i,value;
    for(x=0; x<pw; x++) {
        v=PyList_GetItem(pixlist,x);
        value=0;
        for(i=0; i<4; i++)
            value|= ((PyInt_AsLong(PyTuple_GetItem(v,i))&0xff)<<(i*8));
        mem[x]=value;
    }
    return 1;
}


// CALLABLE FOM PYTHON
// setlosmap(los_map, scalefactor)
// just stores the los_map into the C module
static PyObject *setlosmap(PyObject *self, PyObject *args) // just reads the data
{
    PyObject *l;

    if(!PyArg_ParseTuple(args, "Oi", &l, &sf))
        return NULL;

    if(map!=NULL) free(map);
    rdlosmap(l, &map, &width, &height);

    printf ("ccivil: setlosmap %d %d sf=%d\n",width,height,sf);

    return Py_BuildValue("i",1);

}

// CALLABLE FROM PYTHON
// the main function
// it's the same algorithm from your code
// I hope there are not bugs ...
// "jump checking is disabled because I wanted to see where the vision stops
// It runs very fast on my machine, anyway

static int nchecks;

static int xlim,ylim;
static int trlos(int xs, int ys, int xe, int ye, int debug) // in los coordinates
{
    int zs, ze, dz, zgrd, hmap;
    int dx, dy, adx, ady;
    int iters, t, code;
    //  int p,g;

    int lospoints, lpmod, ttype, cansee;
    int x,y,z;
    int mx,my,mz;


    if(xs==xe && ys==ye) return 1; // can see itself

    zs=map[ys*width+xs]&0x0000ffff;
    ze=map[ye*width+xe]&0x0000ffff;

    if(debug)printf("ccivil.tracelos: from (%d,%d,%lf) to (%d,%d,%lf)\n",
                        xs,ys,zs/50.0, xe,ye,ze/50.0);

    zs += 100;
    ze += 50; // in real height this is +2, +1

    dx=xe-xs;
    dy=ye-ys;
    dz=ze-zs;

    adx=abs(dx);
    ady=abs(dy);

    lospoints=8*1000;
    if(adx>ady) {
        iters=adx;
        lpmod=500*ady/adx;
    } else {
        iters=ady;
        lpmod=500*adx/ady;
    }

    // I've choosen los=7.5 instead of 4.6=ln(100)
    lospoints= 8000000 / (1000+lpmod);

    mx=(dx<<16)/iters;
    my=(dy<<16)/iters;
    mz=(dz<<16)/iters;

    x=xs<<16;
    y=ys<<16;
    z=zs<<16;

    cansee = 1;

    for(t=1; t<=iters; t++) {
        nchecks++;

        code=map[(y>>16)*width + (x>>16)]; //x+=mx; y+=my;
        hmap=code & 0x0000ffff;
        zgrd = ((z>>16) - hmap); //z+=mz;
        ttype=(code&0x00ff0000)>>16;

        if(debug) printf("%c",ttype);

        if(zgrd<0) {
            cansee=0;
            if(debug)printf("\nccivil: under ground: z=%lf map=%lf\n",(z>>16)/50.0, hmap/50.0);
            break;
        }

        switch(ttype) {
        case 'w':
            if(zgrd<10*50) lospoints-=1600;
            break;
        case 'o':
            if(zgrd<5*50) lospoints -=220;
            break;
        }
        if(lospoints<0) {
            cansee=0;
            if(debug)printf("\nccivil: no more points: ter. is %c\n",ttype);
            break;
        }

        // only for step by step !
        x+=mx;
        y+=my;
        z+=mz;

    }

    if(debug)printf("\nccivil: cansee: %d\n",cansee);
    xlim=x>>16;
    ylim=y>>16;
    return cansee;
}

// CALLABLE FROM PYTHON
//tracelos((xunit,yunit),(xtarget,ytarget))
//returns 1 if unit can see target
static PyObject *tracelos(PyObject *self, PyObject *args)
{
    int xs,ys,xe,ye,cansee;

    if(!PyArg_ParseTuple(args, "(ii)(ii)", &xs,&ys,&xe,&ye))
        return NULL;

    xs/=sf;
    ys/=sf;
    xe/=sf;
    ye/=sf;

    if (xs<0 || xs>=width || ys<0 || ys>=height ||
            xe<0 || xe>=width || ye<0 || ye>=height) {
        printf("ccivil.tracelos: range error\n");
        return Py_BuildValue("i",0);
    }

    cansee=trlos(xs,ys,xe,ye,0); // no debug
    return Py_BuildValue("i",cansee);
}

// CALLABLE FROM PYTHON
//(x,y)=loslimit((xunit,yunit),(xtarget,ytarget))
//the same thing but returns the last point where the vision stops
static PyObject *loslimit(PyObject *self, PyObject *args)
{

    int xs,ys,xe,ye;

    if(!PyArg_ParseTuple(args, "(ii)(ii)", &xs,&ys,&xe,&ye))
        return NULL;

    if(trlos(xs/sf,ys/sf,xe/sf,ye/sf,1)) return Py_BuildValue("(ii)",xe,ye);
    return Py_BuildValue("(ii)",xlim*sf,ylim*sf);

}

static int glb(int x, int y) // getlosbit
{
    if(x<0 || x>=width || y<0 || y>=height) return 1;
    return map[y*width+x] & 0x80000000;
}

static int nlines;
static PyObject *newline(int x1,int y1, int x2, int y2)
{
    nlines++;
    return Py_BuildValue("(iiii)",x1*sf,y1*sf,x2*sf,y2*sf);
}

//CALLABLE FROM PYTHON
//getlosarea(xunit,yunit)
//returns a list of 4tuples of form (x1,y1,x2,y2)
//so it's a set of small lines
//if darwn, you can see the outline of the visible regions
static  PyObject *getlosarea(PyObject *self, PyObject *args)
{
    int x,y;
    int i,j;

    PyObject *l;

    if(!PyArg_ParseTuple(args, "(ii)", &x,&y))
        return NULL;

    x/=sf;
    y/=sf;

    if (x<0 || x>=width || y<0 || y>=height) {
        printf("range error: %d %d\n",x,y);
        return PyList_New(0);
    }

    nchecks=0;
    for(i=0; i<width; i++)
        for(j=0; j<height; j++) {
            if(trlos(x,y,i,j,0))
                map[j*width+i]|=0x80000000; //sets bit
            else
                map[j*width+i]&=0x7ffffff; // resets bit
        }
    printf("ccivil.getlosarea: poins checked:%d\n",nchecks);


    // ok, everything is computed

    l=PyList_New(0);
    nlines=0;

    // vert lines
    for(j=0; j<height-1; j++)
        for(i=0; i<width; i++)
            if(glb(i,j)&&glb(i,j+1))
                if((!glb(i-1,j)&&!glb(i-1,j+1)) || (!glb(i+1,j)&&!glb(i+1,j+1)))
                    PyList_Append(l,newline(i,j,i,j+1));


    // hor lines
    for(i=0; i<width-1; i++)
        for(j=0; j<height; j++)
            if(glb(i,j)&&glb(i+1,j))
                if((!glb(i,j-1)&&!glb(i+1,j-1)) || (!glb(i,j+1)&&!glb(i+1,j+1)))
                    PyList_Append(l,newline(i,j,i+1,j));


    // / lines
    for(j=1; j<height; j++)
        for(i=0; i<width-1; i++)
            if(glb(i,j)&&glb(i+1,j-1))
                if(!glb(i,j-1) || !glb(i+1,j))
                    PyList_Append(l,newline(i,j,i+1,j-1));

    // \ lines
    for(j=0; j<height-1; j++)
        for(i=0; i<width-1; i++)
            if(glb(i,j)&&glb(i+1,j+1))
                if(!glb(i,j+1) || !glb(i+1,j))
                    PyList_Append(l,newline(i,j,i+1,j+1));


    //  printf("%d NLINES\n",nlines);

    return l;

}


// this is the main terrain recognition function
// it is JUST A TEST
static int terfromrgb(int r,int g,int b, int code, int codec,int codep, int coden) // codes are precious hints
{
// codes are: g=grass,s=and,m=mud,c=crackedmud,a=water,r=riv,f=ford,p=road, o=rocks, w=wood

    float lum, rr, gg, bb;

    //printf("type %c : r=%d g=%d b=%d\n", code, r,g,b);

    if(codec=='~') codec=code;

    lum=r+g+b;
    if(lum>0) {
        rr=r/lum;
        gg=g/lum;
        bb=b/lum;
    } else rr=gg=bb=0.3;
    lum/=(255*3);

    //lum=0.0 to 1.0, rr, gg, bb from 0.0 to 1.0

    if(code=='o') {
        if (rr<0.5 && gg<0.5 && bb<0.5 ) return 'o';    // rocks
        else return 'g';
    }
    if(code=='m') {
        if (rr>bb && rr>2*bb) return 'm';    // mud
        else return 'g';
    }

    if(gg>0.5) {// is green !
        if(code==codep && code==coden && code==codec) return code;
        if(code!='w' && codep!='w' && coden!='w' && codec!='w') return 'g';
        if(lum<0.11) return 'w';
        else return 'g';
    }

    if(bb>0.5) return 'r'; // water

    if(rr>bb && rr>2*bb) return 'p'; // seems to be a road

    return '#';
}

static int tervalue(int *tmap, int *pixmem, int x, int y) // x and y are in losmap coordinates
{

    int xx,yy;

    int rs,gs,bs, pix, code, ttype, ttypec, ttypep, ttypen, rcode;

    // the pixels are wrong !

    rs=gs=bs=0;
    for(yy=0; yy<sf; yy++)
        for(xx=0; xx<sf; xx++) {
            pix=pixmem[(y*sf+yy)*(width*sf) + (x*sf+xx)];
            rs += (pix & 0xff);
            gs += ((pix>>8) & 0xff);
            bs += ((pix>>16) & 0xff);
        }
    rs/=(sf*sf);
    gs/=(sf*sf);
    bs/=(sf*sf);
    code=tmap[y*width+x];
    ttype  = ((code>>26) & 0x3f)+'A';
    ttypec = ((code>>20) & 0x3f)+'A';
    ttypep = ((code>>14) & 0x3f)+'A';
    ttypen = ((code>> 8) & 0x3f)+'A';

    rcode=terfromrgb(rs,gs,bs,ttype,ttypec, ttypep,ttypen); // should also read iconid

    return rcode;

}

//CALLABLE FROM PYTHON
//createlosmap()
//modifies the losmap by smoothing the terrain and assigning the "real" terrain types
//also stores the data into the C module, so setlosmap has not to be called after it
static PyObject *createlosmap(PyObject *self, PyObject *args) // loslist, pixlist
{
    PyObject *ly, *lx;
    int op, xarg, yarg;
    static int *pixmem, *tmap, pw, ph;
    int x,y;
    int ttype;
    int sum,np,dx,dy,xx,yy;

    if(!PyArg_ParseTuple(args, "iOii", &op, &ly, &xarg, &yarg))
        return NULL;

    if(op==0) { //reads the los_map, stores mainmap height, mallocs pixmap
        pw=xarg;
        ph=yarg;
        if(!rdlosmap(ly, &tmap, &width, &height)) return NULL; // reads the losmap (augumented original format)
        sf= pw / width;
        if((pixmem=(int*)malloc(pw*ph*sizeof(int)))==NULL) {
            printf("ccivil.create map: cant malloc pixmap\n");
            return NULL;
        }
        return Py_BuildValue("i",1);
    }

    if(op==1) { //reads a line from the pixmap
        if(!rdpixline(ly, pixmem+yarg*pw, pw)) return NULL;
        return Py_BuildValue("i",1);
    }

    // op=2 execute !

    printf ("ccivil:createlosmap: %d %d : %d %d sf=%d\n",width, height, pw, ph, sf);

    if(map!=NULL) free(map);
    if((map=(int*)malloc(width*height*sizeof(int)))==NULL) {
        printf("ccivil.create map: cant malloc map\n");
        return NULL;
    }

    // smooths the map
    for(y=0; y<height; y++)
        for(x=0; x<width; x++) {
            sum=np=0;
            for(dx=-16; dx<=16; dx++) {
                xx=x+dx;
                if(xx<0 || xx>=width)continue;
                for(dy=-16; dy<=16; dy++) {
                    if(dx*dx+dy*dy>16*16)continue;
                    yy=y+dy;
                    if(yy<0 || yy>=height) continue;
                    sum += tmap[yy*width+xx] & 0x000000ff; // from original format
                    np++;
                }
            }
            map[y*width+x]= (sum*250/np) & 0x0000ffff; // 0 0 h h; h is realh*50
        }

    // now computes the real terrain value - the real pain

    for(y=0; y<height; y++)
        for(x=0; x<width; x++) {
            ttype=tervalue(tmap,pixmem,x,y);
            map[y*width+x] |= ((ttype&0xff) << 16);
        }

    free(tmap);
    free(pixmem);

    // update the python data structure
    for(y=0; y<height; y++) {
        lx=PyList_GetItem(ly,y);
        for(x=0; x<width; x++)
            PyList_SetItem(lx,x,Py_BuildValue("i",map[y*width+x]));
    }

    printf("ccivil: map created\n");

    return Py_BuildValue("i",1);
}

static PyMethodDef ccivilMethods[] = {
    {"createlosmap", createlosmap,  METH_VARARGS, "creates the los map"},
    {"setlosmap", setlosmap, METH_VARARGS, "Inits the los map"},
    {"tracelos", tracelos, METH_VARARGS, "Verifies the los between 2 poins"},
    {"getlosarea", getlosarea, METH_VARARGS, "List of lines - los area"},
    {"loslimit", loslimit,  METH_VARARGS, "Where does the los stop?"},

    {NULL, NULL, 0, NULL}
};

void initccivil(void)
{
    map=NULL;
    (void) Py_InitModule("ccivil",ccivilMethods);
}


