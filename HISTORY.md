# History of Civil

## Civil on SourceForge (2000-2004)

The original [Civil](http://civil.sourceforge.net/) project was registered [on SourceForge](https://sourceforge.net/projects/civil/)
on 2000-08-31 with the aim to create a real time strategy game about battles in the American Civil War
allowing to play over a network. It was programmed in Python using PyGame as game framework
and some Python C extensions for speeding up critical parts.
It was published under the open source GPL-2.0 license. The game program code was stored in a CVS
version control system.

- Last release: [Civil 0.83](https://sourceforge.net/projects/civil/files/civil/0.83/)
- Authors: Jan 'Chakie' Ekholm, Gareth 'The Corruptor' Noyce, Michael 'mikee' Earl, John Eikenberry,
  Marcus Alanen, Cristian Soviani, Uwe Hermann, Frank Raiser, Mike Szczerban, Jeroen 'slm' Vloothuis, Kalle

## Civil on GitHub (2018-)

On 2017-12-21, Trilarion created the [Civil](https://github.com/Trilarion/civil) project on Github as
a continuation of the Civil project on SourceForge. The sources of the last available release (0.80-0.83)
as well as a snapshot of the CVS repository were stored in a git repository on GitHub.

No release has yet been made.