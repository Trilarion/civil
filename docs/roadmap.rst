*******
Roadmap
*******

Rough list of things to do before we can release 1.0. The versions are just so
that we have some way of defining what we've done so far and what remains. No
dates are set for the various versions.

0.50:
    * simple combat. Does not need to use all available modifiers nor be
      exactly realistic, as long as it works.

0.60:
    * framework for in-game sound effects
    * ending a game (when does a game end, how does it work)
    * working end-game screen

0.66:
    * fully working turnbased system that works as well as 0.60

0.70:
    * working scenario editor that can be used to create scenarios
    * loading/saving games
    * end-game screen should display statistics and a score
    * finish the new floating status, minimap and orders panes.

0.80:
    * simple AI client that does not crash, may even do something useful
    * better scenario management that makes installing scenarios easier, maybe
      a separate application (civil-admin) to take care of that? [DONE]
    * Updates to the XSLT for Docbook and the Scenario Format [DONE]
    * At least one good scenario (TM)

0.8x:
    * minor version increments as the tasks for 0.90 get completed.
    * simple server-side ai that makes units react to actions, i.e. making
      units fire back at suitable targets and retreat when overwhelmed.
    * Verify: does Line-Of-Sight work?
    * visual notification to the player when the action is being calculated or
      sent over the socket.
    * better combat that uses more of the modifiers, terrains, weapon stats
      etc.
    * Include the character level formatting for the FAQ section of the DB processing. [DONE]
    * Move all HTML streams to XHTML
    * Add table of contents to the Manual XHTML output
    * buildings
    * Light Forests (scattered trees)
    * Nice splash screen for the "Computing Action" dialog

0.85:
    * initial realtime engine up and running

0.90:
    * initial packages for: tar.gz, deb, rpm, osx and windows [DONE]
    * a "credits" screen [Partially complete]
    * sound effects for all in-game actions that should have effects
    * setup and in-game music
    * all required graphics should be completed
    * all code should be done. Only tweaks and bugfixes left before it is
      called 1.0.
    * all dialogs should be fixed so that all widgets etc are properly aligned
      and all looks ok. [DONE]
    * Schemas [Partially DONE]
    * Fix the colour map look up table generation
    * Fix Windows HOME [DONE]
    * Fix windows AI starting [DONE]
    * Loading splash screen
    * Complete the credits screen
    * Missing game over screens
