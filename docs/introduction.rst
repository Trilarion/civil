************
Introduction
************


Welcome the the Civil online help! Here you can get quick information about how to play
Civil. To get a list of the currently active keyboard shortcuts you can press F2.

A little button labeled "Menu" can be seen in one of the windows. Clicking it brings up a menu
of all available options.

What's on the map?
------------------

The main in-game display in Civil contains the map and a few extra features. The map is where
all units are shown, along with the orders they have. Union units are shown using blue icons,
and confederate units with gray/brown icons. A little rectangle under the unit shows the
facing of the unit.

The map is made up of various types for terrain that affects movement, combat and visibility
if various ways. See "Terrain types" for more info.

A number of windows also can be seen on the map. See "Windows" for more info about these.

Up in the right corner of the map is an area where messages to the player will be shown. These
are informational messages from the game, chat messages from you opponent and other info. The
messages will slowly scroll up and away from the map.

Toggleable unit features in the map
..................................

Several features in the map related to unit info can be toggled on and off, according to how
much information you want. If the map feels cluttered you can turn off the features that you
do not need at the moment, or are not interested in at all.

To select which features are shown press F3. This brings up a dialog where all features can be
individually toggled. See the link below for descriptions of what each of these features are.

Toggleable features window
.........................

This dialog allows you to choose what to display on the map. Select the wanted features and
click Ok.

"Map labels" toggles wether to show some informative geographical labels in the map, such as
village and hill names.

"Objectives" toggles objectives areas on and off.

"Unit symbols" toggles wether to show the rectangular unit symbols. The symbols visualize the
unit facing (the triangle) and strength (size). The symbols can be toggled for own and enemy
units

"Unit icons" toggles wether to show the icon representation of the units. This shows cavalry
as men with horses etc. Icons only show two facings and the strength as a little horizontal
bar.

"Unit orders" toggles orders given for own units. Move orders are shown using lines of
different colors etc, attack orders as different lines etc.

"Line of sight" shows an outline around the unit which represents the are of the map visible
to the unit.

"Superior commanders" shows the command chain from the unit up to its commanding officer and
up all the way to the highest level. Use this to make sure that companies are kept close to
their CO.

"Weapon ranges" shows the max effective weapon range for the primary weapon of the units. The
range is shown as a circle around the unit.

Giving orders
-------------

To give orders to a unit you must first select the unit. Do that by clicking on the unit. It
should then be highlighted and you should see some information about the unit in the unit info
window.

The highlighted unit has a circle around it. If several units are selected one unit is the
primary selection. Secondary units have a magenta circle around them.

When a unit is selected the right mouse button will show a popup menu with all available
orders that all the selected units can perform. If one of the selected units can not, say, move
fast, then that order is not available.

Various different features the help visualize the unit and its state can be toggled on and
off. These include line of sight, organization, facing etc.

Minimap
-------

The minimap is a little floating window that contains a miniature version of the map. It is
normally in the upper left corner. A little rectangle indicates which part of the map is
currently shown in the game area.

You can click anywhere in the minimap window to quickly scroll to that part of the map.

Action phase
------------

This is the action phase of Civil. All the orders you have given your units are executed here
and your role is just to observe what happens. You can control the flow of the time using the
controls in the action window.

The right mouse button will bring up a context sensitive popup menu.

To end the action phase press Alt-e. Note that you can end the action phase at any time, you
do not need to watch all of the action before ending.

Action window
-------------

The action window lets you control the flow of time. This is a little floating window that is
normally in the lower right corner of the play area. It is not shown during the orders phase,
only during the action phase.

Units
-----

There are four basic types of units that make up armies in Civil. The units all have different
behaviour and uses. Using all units correctly is the key to a victory.

Unit orders
...........

A little window that always shows information about the orders a firendly unit has. Every
order you have given to the unit is shown as a separate row in this window. Using this window
you can get a quick overview of what the unit is ordered to do. This gets extremely useful if
the map is cluttered or the unit has many "wait" and "change mode" orders.

Artillery
.........

Artillery units make up the long range support units in the Civil War. They bombard enemies
from a distance and are used to prepare the ground for an infantry assault or to scatter
advancing enemies.

Artillery is slow and very vulnerable when attacked from the flank.

Cavalry
.......

Cavalry units are horse mounted fast troops. They are often used for recon and rear are
action. Due to their speed they can travel very fast over vast distances.

Headquarters
............

Commanders for various organizations are placed in headquarter units. This allows a brigade
commander to freely move around the battlefield and to visit and rally regiment, battalions
and companies. All organizations except companies have separate headquarter units. Companies
have the company commander integrated in the company unit.

A headquarter unit can rally nearby routed and disorganized infantry just by its
presence. Headquarters are also vulnerable and very likely skirmish targets.

Infantry
........

Infantry units make up the bulk of the force in a Civil War battle. They are the normal
battling units.

When in combat infantry units will use battle formation, which is suited for getting every
man in the unit to fire. While moving infantry units are in column mode, which is much faster
but also much less suitable for combat.

Infantry is not very vulnerable, but not very effective either, unless the range is really
short. Infantry units move slowly in battle formation and quicker when in column mode.

Unit info
.........

A little window always shows information about the current selected unit. The window is by
default in the bottom left corner of the map. It is a moveable window, which means that you
can move it anywhere you want, and even hide it.

The information in the window includes the unit name, commander info, men and weapon status as
well as various other stats.

If several units are selected then the main selected unit is the one whose info is shown. If
an enemy unit is clicked only its name and type is shown.

Audio controls
--------------

The audio controls is a little floating window that allows you to select the background music
that should be used. You can:

1. listen to the supplied background music by checking the checkbox "Included music".
2. listen to a normal audio CD. Just insert the CD into the CD-ROM and check "CD". You can use
   the controls at the bottom of the window to skip tracks, pause etc.
3. no music at all. Just check "No music".

Saving games
------------

The game can be saved at any time during the orders phase, but not in the action phase. To
save a game press Alt-s. This brings up a dialog asking for confirmation and a filename. Enter
the wanted name and click "Ok" to save. Click "Cancel" to abort the saving. The game continues
normally after a save.

Quitting Civil
--------------

To quit Civil just press the keys Alt-q. You will be asked for confirmation first. You may
also want to save your game before quitting.
