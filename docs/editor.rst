******
Editor
******

* Scenario editor

** Introduction

The scenario editor is a tool that allows the user to create scenarios for Civil. All data for a
scenario can be produced in the editor, thus making it simpler to create scenarios. You can edit
the map, units (and their initial positions) as well as any needed background information.

The editor can save scenarios in a format that is directly useable by Civil and it can also edit any
other existing scenario.


** Requirements

To run the scenario editor the following components should be installed:

 - PyQt (http://www.riverbankcomputing.co.uk/pyqt/) -- the Python wrapper for Qt.
 - Qt (http://www.trolltech.com/)
 - Pygame (http://www.pygame.org)


* Creating a new scenario

To create a brand new scenario choose "New" from the "File" menu. You will be asked for confirmation
so that you don't accidentally lose work on an open scenario. Answer "No" to cancel
the creation of a new scenario.

After this you are shown a dialog where you enter the basic information for a scenario.


** Scenario theatre

The theatre defines where the battle takes place, in which war, and at which time in history.
Currently Civil is very focused on the US Civil War, but other theatres will be added at a later
time. The theatre defines what kind of weapons and units can be used in order to make a scenario
more historically correct.

Choose the wanted theatre from the dropdown box.

** Map size

The map size setting defines the size of the playfield. The size is given in hexes, like the ones
that you see on the left in the "Icons" tab when starting the editor. Each hex is roughly 50m wide
and 50m high, so 20 hexes is 1km, and 100 hexes thus 5km. Do not create large maps unless it is needed
for the scale of the battle you are designing.


** Saving a scenario

Remember to save your scenario regularly! The editor is pretty stable, but in order to avoid losing
work please take care to save your work often.

To save, select "Save As" from the "File" menu and give your scenario an initial name. Select where to
save it and then click "Save".

Civil uses the extension .civil for scenarios, make sure you do not save with any other extension, as
Civil will not be able to use those scenarios.

After the scenario has been saved once you can use the normal "Save" from the "File" menu or press
Control and S to save using the previous name.


* Opening an existing scenario

To open an old scenario for editing, use "Open" from the "File" menu. Pick a scenario to open.
Opening a scenario erases any unsaved changes in the current scenario, so remember to save before
opening a new scenario.


* Map

Editing the map is the most central part of the scenario editor. The map is created from
graphical hexes, each of which depicts a different terrain or feature.

All currently available hexes are shown in the "Icons" tab. To select an icon just click on it.
A tooltip is available for each icon that tells you its id. To place an icon on the map just click within
the map itself.


** Grid

By default a grid of white dots is drawn on the map. The dots represent the center of each hex and
are supposed to make it easier to place hexes. Especially if large areas of, for instance, grass are
placed it can be hard to see where each hex is. The grid can be toggled on and off with the option "Grid"
from the "Tools" menu, or by pressing Control and g.


** Tips

Try to make variations in the terrain. There are a lot of different grass, woods etc icons that make
this possible. A map where all grass is drawn using one single icon gets very boring to look
at. When a new scenario is started the map is filled with "smooth" grass.


** Plugins and blocks

There are a few plugins and block functions that make the editing job a bit more easy. See later
sections about "Blocks" and "Plugins".


* Units

The units are of course a very important part of a scenario, and they make up the "playable" content
for the players. Civil allows the designer to build up a realistic unit composition that closely
matches a real battle. The units are formed into normal military organizations, with brigade being
the largest organization and companies the smallest. A player can actually only move companies,
never any larger organizations. A brigade is a logical entity that is represented on the battlefield
as a headquarter unit.


** Organizations

The available organizations are:

    o brigade
    o regiment
    o battalion
    o company

A brigade contains a brigade HQ and a number of regiments. A regiment contains a regiment HQ and a
number of battalions or alternatively a number of companies. A battalion has a HQ and a number of
companies. The designer should take care to try to model realistic organizations to give the players
an interesting scenario.


** Unit types

A scenario contains up to four different unit types that can actually be controlled. These types are
all used differently and a realistic army is composed of all of them. The types are:

    o infantry
    o cavalry
    o artillery
    o headquarter

Infantry units make up the bulk of the army, they are the grunts that are used to get things
done. A company of infantry is up to 100 men, but usually less due to casualties and missing
reinforcements.

A cavalry unit is the same as infantry, except that the men are mounted on horses.

An artillery company is actually an artillery battery. It contains a battery of guns along with the
personnell needed to operate them and move the horses needed to move them. Artillery has a support
role in the army.

Headquarter units represent the commander for an organization along with his staff. These units are
small and have an administrative role in the battle. Each battalion, regiment and brigade has an
associated HQ unit with the commanding officer. The HQs should be placed on the map pretty close to
the units in the organization.


** Creating units

To create units first activate the "Rebel units" or "Union units" tab. Right-click in the tab and
choose the organization that should be created. At first you can only create brigades. Creating a
new brigade will create the brigade organization along with a headquarter unit. To add other lower
organizations to the brigade highlight the brigade (not the HQ) and choose "Create regiment" from
the popup menu, which will create a regiement and a regiment HQ. Into the regiment you can create
battalions (along with needed HQs) or infatry, cavalry and artillery companies.

Organizations can also be copied in order to make it easier to duplicate large organizations. Use
the copy and paste actions from the popup menu for that purpose.


** Deleting units

To delete a unit or a full organization just highlight it and choose "Delete" from the popup menu.


** Editing units

Editing the created units is a very important part in creating the scenario. The units are created
with dummy default values for all data, and a realistic scenario needs the data to be updated to
some more realistic values. Highlight the organization or unit and choose "Edit" from the popup menu
to bring up a dialog for editing data.

For the large logical organizations that are not visible on the map the only thing that can be
edited is the name of the organization.

For units that are visible on the map the amount of data is much larger. The dialog is divided into
four parts: basic data, modifiers, commander and weapon. The basic data is simple: just the
unit name, and the number of men in the unit.

The modifiers determine how the unit will behave on the battlefield. Higher values for morale and
experience is better. These values range from 99 (best) to 0 (worst). Fatigue means how tired the
unit is. Low values are better, ie a fatigue of 0 means the unit is fully rested and 999, that the
unit is so exhausted it can't really do anything anymore.

The commander settings are for the immediate commander in the unit. For a company it defines the
data for the commander in that company, and for a headquarter it defines data for the commander of
that organization. For instance the commander in a brigade HQ unit is the commander for that whole
brigade. For experience, rally skill and motivation higher values are better. Aggressiveness defines
how aggressive the commander is. A high value means the commander is more likely to attack when an
opportunity arises, while a low value indicates a cautious commander.

The weapon data sets the primary weapon for the unit. For artillery units the primary weapon should
be artillery guns, and for normal infantry some kind of rifles. The available weapons depend on the
selected theatre and which weapons have been defined in the "Weapons" tab. Select the proper number
of weapons too. For infantry each man likely has a rifle, but an artillery unit probably has no more
than six guns.


* Placing units on the map

To place a unit on the map, simply highlight it and press the left mouse button within the map where the
unit should be placed. To reposition a unit just click on a new location.

The facing of a unit is shown using the little triangle in the unit. To change the facing of a unit
make sure it is highlighted and then click the middle mouse button in the map. The unit will turn to
face the clicked position.


* Scenario info

A scenario needs some background information in order to be realistic. That information is entered
in the "Scenario info" tab. The information here is not really mandatory for a scenario to actually
work technically, but it is more or less required for cosmetic reasons. The data is divided into
three separate parts.


** Basic information

This is the basic information for a scenario. The data here is information that the players will see
when they choose a scenario. The scenario name is the universal name for the scenario, it will be
used all over Civil. The description is a free text description that is meant to give some
background information regarding the scenario. It does not have to be long, but it should describe
the scenario in a few sentences.

The geographic location is the place where the battle takes place. It can be basically anything that
indicates a location.

The date and time is the historical starting date and time for the battle. It depends on the chosen
theatre.

The number of turns indicate the maximum number of turns until the game ends. If neither player has
won before the max number of turns has been reached then the game ends and calculates a score and a
winner.


** Missions

The section for missions lets the designer write mission objectives for the two players. This text
is a free format text that is shown to the player when a scenario is started, and it should give the
player a rough description as to what they are expected to do. It could refer to objectives,
historical data, locations on the map etc.


** Author information

This is meant to be used in the future, currently it is ignored. It is supposed to store information
about the author of the scenario.


* Objectives

Objectives are special locations on the map that have strategic value and thus need to be captured
and/or held by both players. An objective can be a hill, a bridge, a road crossing, a hill top
etc. Players are supposed to try to capture and hold objectives, as they will be worth points when
the final outcome is calculated. Typically a scenario contains a few objectives, but not too
many. Having too many objectives makes the battles less focused and thus possibly less fun.

Editing objectives takes place in the "Objectives" tab. You can add, remove and edit objectives.


** Adding objectives

To add a new objective choose "New" from the popup menu. This creates an objective with default
values for all parameter. You probably want to edit it after creating it.


** Removing objectives

To remove an obejctive simply highlight it and choose "Delete" from the popup menu.


** Editing objectives

To edit an objective choose "Edit" from the popup menu. This brings up a little dialog where all
data for the objective can be set. The name is a string that will be shown in the map and the
objective position (along with an icon). This should represent the objective and give a short
description about the geographical location, such as "Walkabout crossing", "Aboa village" or "Hill
104". The description is an optional free tetx description of what makes the objective important,
but it is not mandatory.

The owner setting is used to indicate which player is in possession of the objective when the
scenario starts. The default is "Nobody", which means that the objective is neutral and not
owned by any player.

The points define how much the objective is worth when the final game score is calculated. One point
equals to one inflicted casuality, so one can think of the worth of an objective as "how many men is
it worth to sacrifice to reach the objective". Typical values can be 100, 250, 500 and 1000.


** Placing objectives

Placing objectives on the map is simple. Just highlight the objective and click on the map on the
position where the objective should be placed.


* Locations

A location is a place on the map that can be somehow interesting to the player. It is a label given
to a certain geographical feature, but it has no gameplay value. The player can not capture
locations as he/she can do with objectives, they are just labels used for informative purposes. The
reason for locations is to be able to add some realism to a sceanario that the player might be familiar
with, or to let the player know where locations mentioned in the scenario or mission description are
located.

* Weapons

* Blocks

The editor contains a number of predefined blocks of hexes. The blocks are designed to make it
easier to create certain often used features, such as woods and similar. To use blocks activate the
"Blocks" tab and select the wanted block. A preview is shown in the bottom half of the tab. To use
the block just highlight the wanted block and click in the map. The left upper corner of the block
will be placed at the mouse position. Note that if the block contains empty icons the first really
painted icon may be below and/or to the right of the clicked position.


** Defining new blocks

Currently defining new blocks is a matter of defining an XML file. When the editor starts up it
scans a directory and loads all XML files that define blocks. The format is simple, it is basically
a series of coordinates that define which icon to put where. A simple example:

<?xml version="1.0"?>

<block>
  <name>Small patch of woods</name>
  <icons>
    <icon x="0" y="1" id="228"/>
    <icon x="0" y="2" id="226"/>
    <icon x="1" y="2" id="227"/>
  </icons>
</block>

The <name> is mandatory and the icons are defined in a series of <icon> tags. The id attribute
refers to an icon id. See the tooltips in the icon palette for the icon id:s. A block does not have
to define solid ranges of icons, it can leave "holes" if needed and have an arbitrary shape. The
locations that are empty will just not be filled in when the block is used.

Save new blocks into TODO: where?

* Plugins

The editor can be extended with fucntionality defined in plugins. A plugin when activated allows the
user to do some operations on the map, and it can then create or manipulate the proper data
according to needs.

Activate a plugin by selecting the "Plugins" tab and then highlighting the wanted plugin from the
list.

** Road creator

The road creator is used to create roads. Creating them manually from single hexes can be tedious,
and this plugin allows the user to just draw the path for the road. Click and hold down the left
button in the map where the road should start, and slowly drag along the path of the road. The
plugin will try to place suitable road hexes along the road path. When done with the road, release
the left mouse button.

You may need to do some small touch ups after the plugin has finished, especially if some crossing
is needed.

** River creator

This plugin works similarly to the road creator, but it creates rivers instead of roads.

** Hill creator

The hill creator allows the user to create hills in the same way as roads and rivers. The outline of
the hill is simply traced and the plugin puts the needed hexes in the map.

** Forest creator

Works in the same way as the hill creator plugin.
