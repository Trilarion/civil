Civil's documentation!
======================

Civil is a real-time strategy game allowing players to take part in scenarios set
during the American Civil war.

.. toctree::
   :maxdepth: 2
   
   introduction
   playing
   history
   installation
   development
   roadmap

* :ref:`genindex`
* :ref:`search`

Built on |today|
