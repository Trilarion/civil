# coding=utf-8
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

"""

"""

import os
import sys

def main():
    """
    Main entry point.
    """

    # Add the parent directory of the package directory to Python's search path.
    # This allows the import of the 'civil' modules, even if the current working
    # directory is not part of Python's search path by default.
    source_directory = os.path.realpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.pardir))
    if source_directory not in sys.path:
        sys.path.insert(0, source_directory)

    # get user folder
    from civil.base import constants
    user_folder = constants.USER_FOLDER




if __name__ == '__main__':

    main()