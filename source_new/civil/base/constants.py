# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
Game specific path locations for artwork, music, ...
Only static values here.
"""

import os

from civil import version


def extend(path, *parts):
    """
    Uses os.path.join to join parts of a path. Also checks for existence and raises an error
    if the path is not existing.
    """
    extended = os.path.join(path, *parts)
    if not os.path.exists(extended):
        raise RuntimeError('constructed path {} does not exist'.format(extended))
    return os.path.abspath(os.path.realpath(extended))


def determine_user_folder():
    """
    Determines the location of the user folder. Creates it if not existing.
    """
    if os.name == 'posix':
        # Linux / Unix
        # see 'XDG_CONFIG_HOME' in https://specifications.freedesktop.org/basedir-spec/
        config_basedir = os.getenv('XDG_CONFIG_HOME', os.path.join(os.path.expanduser('~'), '.config'))
        user_folder = os.path.join(config_basedir, version.APPLICATION_NAME)
    elif (os.name == 'nt') and (os.getenv('USERPROFILE') is not None):
        # MS Windows
        user_folder = os.path.join(os.getenv('USERPROFILE'), version.APPLICATION_NAME)
    else:
        user_folder = os.path.join(os.path.expanduser('~'), version.APPLICATION_NAME)

    # if not existing, create user folder
    if not os.path.isdir(user_folder):
        os.mkdir(user_folder)

    return os.path.abspath(user_folder)


APPLICATION_NAME = 'civil'

USER_FOLDER = determine_user_folder()